package cuentas.back.general.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.general.modelo.Marca;
import cuentas.back.general.services.MarcaServices;

@RestController
public class ControlMarca {
	
	@Autowired
	private MarcaServices serviceMarca;
	
	@GetMapping("/marca/listarMarca")
	public List<Marca> listarMarca(Model model) {
		return serviceMarca.listar();
	}
	
	@GetMapping(value = "/marca/porRegio/{id}")
	public List<Marca> buscaMarcasPorRegion(@PathVariable int id){
		return serviceMarca.marcaPorRegion(Integer.valueOf(id)) ;
	}
	
	@GetMapping(value = "/marca/porDesc/{descripcion}")
	public Marca buscaMarcaDescripcion(@PathVariable String descripcion) {
		return serviceMarca.marcaPorDescripcion(descripcion);
	}
}
