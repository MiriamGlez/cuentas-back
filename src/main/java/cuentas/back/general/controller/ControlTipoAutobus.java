package cuentas.back.general.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.general.modelo.TipoAutobus;
import cuentas.back.general.services.TipoAutobusServices;

@RestController
public class ControlTipoAutobus {

	@Autowired
	private TipoAutobusServices servicesTipoBus;

	@GetMapping("/tipoBus/consulta")
	public List<TipoAutobus> consultaTipoBus(){ 
		return (List<TipoAutobus>) servicesTipoBus.consultaTipoBus();
	}
}
