package cuentas.back.general.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.general.modelo.Zona;
import cuentas.back.general.services.ZonaServices;

@RestController
public class ControlZona {

	@Autowired
	private ZonaServices servicesZona;
	
	@GetMapping("/zona/consulta")
	public List<Zona> consultaZonas(){ 
		return (List<Zona>) servicesZona.consultarZona();
	}
	
	@GetMapping(value = "/zona/porMarcas/{id}")
	public List<Zona> actualiza(@PathVariable int id){
		return servicesZona.consultaZonasMarcas(Integer.valueOf(id)) ;
	}
	
	
}
