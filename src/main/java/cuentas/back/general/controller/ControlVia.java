package cuentas.back.general.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.general.modelo.Via;
import cuentas.back.general.services.ViaServices;

@RestController
public class ControlVia {

	@Autowired
	private ViaServices viaServices;

	@GetMapping("/via/consultaVia")
	public List<Via> consultaVia(Model model) {
		return viaServices.consultaVias();
	}

	@GetMapping(value = "/via/porDescripciones/{descVias}")
	public List<Via> buscaViasPorDescripcion(@PathVariable String descVias) {
		List<Via> via = viaServices.buscaViasPorDescripcion(descVias);
		return viaServices.buscaViasPorDescripcion(descVias);
	}

}
