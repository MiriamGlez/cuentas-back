package cuentas.back.general.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.general.modelo.Region;
import cuentas.back.general.services.RegionServices;

@RestController
public class ControlRegion {
	
	@Autowired
	private RegionServices servicesRegion;
	
	@GetMapping("/region/consulta")
	public List<Region> consultaRegion(){ 
		return servicesRegion.consulta();
	}
}
