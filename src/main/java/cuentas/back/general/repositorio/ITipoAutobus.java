package cuentas.back.general.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.general.modelo.TipoAutobus;

@Repository
public interface ITipoAutobus extends CrudRepository<TipoAutobus, Integer>{

}
