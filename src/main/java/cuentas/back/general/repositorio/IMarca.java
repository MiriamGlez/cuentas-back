package cuentas.back.general.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.general.modelo.Marca;

@Repository
public interface IMarca extends CrudRepository<Marca, Integer>{

	@Query("select a from Marca a where a.status = ?1")
	List<Marca> marcasPorEstatusActivo(Integer estatus);
	
	@Query("select x.marca from RegionMarca x  where x.marca.status = 1  and x.region.id = ?1 order by x.marca.descMarca asc ")
	List<Marca> marcasPorRegion(Integer IdRegion);

	@Query("select a from Marca a where a.descMarca like ?1")
	Marca marcaPorDescripcion(String descripcion);
	
}
