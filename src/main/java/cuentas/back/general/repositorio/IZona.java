package cuentas.back.general.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.general.modelo.Zona;

@Repository
public interface IZona extends CrudRepository<Zona, Integer>{

	@Query("select x.zona from ZonaMarca x  where x.zona.status = 1  and x.marca.id = ?1 order by x.zona.descZona asc")
	List<Zona> zonasPorMarca(Integer id);
}
