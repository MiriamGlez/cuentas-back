package cuentas.back.general.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.general.modelo.Region;

@Repository
public interface IRegion extends CrudRepository<Region, Integer>{

}
