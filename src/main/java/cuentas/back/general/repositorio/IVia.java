package cuentas.back.general.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cuentas.back.general.modelo.Via;

@Repository
public interface IVia extends CrudRepository<Via, Integer> {

	@Query("SELECT  x FROM Via x WHERE x.status=1 AND x.nombVia IN (:descVias ) ")
	List<Via> buscaViasPorDescripcion(@Param("descVias") String descVias);
}
