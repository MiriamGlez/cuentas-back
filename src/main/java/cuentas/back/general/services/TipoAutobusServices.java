package cuentas.back.general.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.general.modelo.TipoAutobus;
import cuentas.back.general.repositorio.ITipoAutobus;

@Service
public class TipoAutobusServices {

	@Autowired
	private ITipoAutobus data;
	
	public List<TipoAutobus> consultaTipoBus(){ 
		return (List<TipoAutobus>) data.findAll();
	}
}
