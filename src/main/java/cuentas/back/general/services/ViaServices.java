package cuentas.back.general.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.general.modelo.Via;
import cuentas.back.general.repositorio.IVia;

@Service
public class ViaServices {

	@Autowired
	private IVia data;
	
	public List<Via> consultaVias(){ 
		return (List<Via>) data.findAll();
	}

	public List<Via> buscaViasPorDescripcion(String descVias) {
		List<Via> lst = data.buscaViasPorDescripcion(descVias);
		return (List<Via>) data.buscaViasPorDescripcion(descVias);
	}
}
