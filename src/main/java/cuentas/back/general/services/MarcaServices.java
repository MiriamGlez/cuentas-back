package cuentas.back.general.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.general.modelo.Marca;
import cuentas.back.general.repositorio.IMarca;

@Service
public class MarcaServices {

	@Autowired
	private IMarca data;
	
	public List<Marca> listar() {
		return (List<Marca>)data.findAll();
	}

	public List<Marca> marcaPorRegion(Integer id) {
		return (List<Marca>)data.marcasPorRegion(id);
	}
	
	public int save(Marca marca) {
		return 0;
	}

	public void delete(int id) {
	}

	public Marca marcaPorDescripcion(String descripcion) {
		return data.marcaPorDescripcion(descripcion);
	}
}
