package cuentas.back.general.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.general.modelo.Region;
import cuentas.back.general.repositorio.IRegion;

@Service
public class RegionServices {

	@Autowired
	private IRegion data;
	
	public List<Region> consulta(){ 
		return (List<Region>) data.findAll();
	}
}
