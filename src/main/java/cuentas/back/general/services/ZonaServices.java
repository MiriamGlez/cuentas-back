package cuentas.back.general.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.general.modelo.Zona;
import cuentas.back.general.repositorio.IZona;

@Service
public class ZonaServices {

	@Autowired
	private IZona data;
	
	public List<Zona> consultarZona(){ 
		return (List<Zona>)data.findAll();
	}
	
	public List<Zona> consultaZonasMarcas(Integer id){ 
		return (List<Zona>) data.zonasPorMarca(id);
	}
}
