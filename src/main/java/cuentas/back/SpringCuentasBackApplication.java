package cuentas.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCuentasBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCuentasBackApplication.class, args);
	}

}
