package cuentas.back.ccuentas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.Rol;
import cuentas.back.ccuentas.services.RolServices;

@RestController
public class ControlRol {

	@Autowired
	private RolServices rolServicio;
	
	@PostMapping("/rol/save")
	public ResponseEntity<String> save(@RequestBody Rol rol){
		rolServicio.guardaRol(rol);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
}
