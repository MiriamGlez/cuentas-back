package cuentas.back.ccuentas.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.RolOferta;

@RestController
public class ControlGeneraDetRol {

	@PostMapping("/oferta/armaDetalle")
	public ResponseEntity<List<?>> guarda(@RequestBody RolOferta rol) {
		List<Object> lst = new ArrayList<>();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy mm:ss");
			SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
			Date fInicio = rol.getHoraInicio();
			Date fTermino = rol.getHoraTermina();
			String fec = sdf.format(rol.getFrecuencia());
			Date frecuencia = null;

			frecuencia = sdf.parse(fec);

			while (fInicio.before(fTermino)) {
				lst.add(rol.getIdRol());
				lst.add(rol.getCveOrigen());
				lst.add(rol.getCveDestino());
				lst.add(fInicio);
				lst.add(rol.getDescVia());
				lst.add(rol.gettRecorrido());
				lst.add(rol.getKms());
				Date horaLlegada = sumaHrsMin(fInicio, rol.gettRecorrido());
				lst.add(horaLlegada);

				fInicio = sumaHrsMin(fInicio, frecuencia);
				System.out.println(rol.getIdRol() + " -- " + rol.getCveOrigen() + " -- " + rol.getCveDestino() + " -- "
						+ sdf1.format(fInicio) + " -- " + rol.getDescVia() + " -- " + sdf1.format(rol.gettRecorrido())
						+ " -- " + rol.getKms() + " -- " + sdf1.format(horaLlegada));

			}
			System.out.println();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<?>>(lst, HttpStatus.OK);
	}

	private Date sumaHrsMin(Date dFecha, Date dSumar) {
		Date miSuma = null;
		Calendar oC = Calendar.getInstance();
		oC.clear();
		oC.setTimeInMillis(dFecha.getTime());
		Calendar cSumar = Calendar.getInstance();
		cSumar.clear();
		cSumar.setTimeInMillis(dSumar.getTime());
		int horas = cSumar.get(Calendar.HOUR_OF_DAY);
		int mins = cSumar.get(Calendar.MINUTE);
		oC.add(Calendar.HOUR, horas);
		oC.add(Calendar.MINUTE, mins);
		miSuma = oC.getTime();
		return miSuma;
	}
}
