package cuentas.back.ccuentas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.DetalleTramo;
import cuentas.back.ccuentas.services.DetalleTramoServices;

@RestController
public class ControlDetalleTramos {

	@Autowired
	private DetalleTramoServices serviceDetTramo;
	
	@RequestMapping("/detalleTramo/busqueda/{idTramo}")
	public List<DetalleTramo> buscaParadasPorCveDesc(@PathVariable Integer idTramo) {
		return serviceDetTramo.detalleListaPorTramo(idTramo);
	}
	
	@GetMapping("/detalleTramo/lista")
	public List<DetalleTramo> listaDetalles(){
		return (List<DetalleTramo>) serviceDetTramo.consultar();
	}

	
}
