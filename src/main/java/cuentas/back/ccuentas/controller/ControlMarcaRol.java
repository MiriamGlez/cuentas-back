package cuentas.back.ccuentas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.ccuentas.services.MarcaRolServices;

@RestController
public class ControlMarcaRol {
	
	private Integer rolId = null;

	@Autowired
	private MarcaRolServices serviceMarcaRol;
	
	@GetMapping("/marcaRol/listarMarcaRol")
	public List<MarcaRol> listarMarcaRol(Model model) {
		return serviceMarcaRol.consultar();
	}

	@PostMapping("/marcaRol/save")
	public ResponseEntity<String> save(@RequestBody MarcaRol marcaRol) {
		if(getRolId() != null) {
			marcaRol.setRolId(getRolId());
		}
		serviceMarcaRol.guardar(marcaRol);
		setRolId(null);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/eliminar/{rolId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable int rolId) {
		serviceMarcaRol.delete(rolId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/update/{rolId}")
	public ResponseEntity<MarcaRol> update(@PathVariable int rolId) {
		Optional<MarcaRol> marcaRol = serviceMarcaRol.listarId(rolId);
		MarcaRol marca = marcaRol.get();
		setRolId(marcaRol.get().getRolId());
		return new ResponseEntity<MarcaRol>(marca, HttpStatus.OK);
	}

	
	@GetMapping(value = "/marcaRol/porDescripcion/{descripcionMarcarol}")
	public MarcaRol buscaMarcaRolPorDescripcion(@PathVariable String descripcionMarcarol) {
		return serviceMarcaRol.buscaMarcaRolPorDescripcion(descripcionMarcarol);
	}
	
	public Integer getRolId() {
		return rolId;
	}
	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}
}
