package cuentas.back.ccuentas.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.DetalleTramo;
import cuentas.back.ccuentas.modelo.DetalleTramoKey;
import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.ccuentas.services.DetalleTramoServices;
import cuentas.back.ccuentas.services.TramosMarcaServices;
import cuentas.back.general.modelo.Marca;

@RestController
public class ControlTramosMarca {

	private Integer idTramo = null;
	@Autowired
	private TramosMarcaServices serviceTMarca;
	
	@Autowired
	private DetalleTramoServices serviceDtramo;
	
	@GetMapping("tramoMarca/consulta")
	public List<TramoMarca> listarTramosMarca(){
		 return serviceTMarca.consultar();
	}
	
	@PostMapping("/tramo/guarda")
	public ResponseEntity<String> guarda(@RequestBody TramoMarca tramo){
		if(getIdTramo() != null) { 
			tramo.setTramoMarcaId(getIdTramo());
		}else { 
			setIdTramo(null);
		}
		serviceTMarca.guarda(tramo);
		
		Vector<Object>lstDetalles = armaDetalle(tramo, getIdTramo());
		serviceDtramo.guardaDetalle(lstDetalles);
		setIdTramo(null);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@GetMapping("tramo/update/{idTramoMarca}")
	public ResponseEntity<TramoMarca> update(@PathVariable Integer idTramoMarca){
		Optional<TramoMarca> tramoMarca = serviceTMarca.listarLlave(idTramoMarca);
		TramoMarca llave = tramoMarca.get();
		setIdTramo(llave.getTramoMarcaId());
		return new ResponseEntity<TramoMarca>(llave, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/tramosMarca/delete/{idTramoMarca}")
	public ResponseEntity<String> elimina(@PathVariable int idTramoMarca){ 
		serviceTMarca.delete(idTramoMarca);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	private Vector<Object> armaDetalle(TramoMarca tramo, Integer tramoId) {
		try {

			Date tRecorrido = (Date) tramo.gettRecorrido();
			Timestamp fInicial = new Timestamp(retormaFechaCeroHoras(new Date()).getTime());
			Timestamp fFinal = new Timestamp(retormaFecha24Horas(new Date()).getTime());
			Vector<Object> vDatos = new Vector<>();
			int dias = 7;
			for (int i = 1; i <= dias; i++) {

				DetalleTramo detalle = new DetalleTramo();
				DetalleTramoKey key = new DetalleTramoKey();

				key.setTramoId(tramo.getTramoMarcaId());
				key.setDiaSemana((String) diasSemana().get(i));
				key.setInicio(fInicial);
				detalle.setFin(fFinal);
				detalle.settRecorrido(new Timestamp(tRecorrido.getTime()));
				detalle.setFecHorAct(new Date());
				detalle.setUsuarioId(1);
				detalle.setKey(key);

				vDatos.add(detalle);
				String cadena = detalle.getKey().getDiaSemana() + " - " + detalle.getKey().getInicio() + " - "
						+ detalle.getFin() + " - " + detalle.gettRecorrido();
				System.out.println(cadena);
			}
			return vDatos;
		} catch (Exception e) {
			return null;
		}
	}

	public Date retormaFechaCeroHoras(Date aFechaDD_MM_YYY) {
		Date myDate = null;
		String fec = timestampToString(new Timestamp(aFechaDD_MM_YYY.getTime()));
		fec = fec + " 00:00:00";
		try {
			SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			myDate = myFormat.parse(fec);
		} catch (Exception e) {
		}
		return myDate;
	}

	public Date retormaFecha24Horas(Date aFechaDD_MM_YYY) {
        Date myDate = null;
        String fec = timestampToString(new Timestamp(aFechaDD_MM_YYY.getTime()));
        fec = fec + " 23:59:59";
        try {
            SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            myDate = myFormat.parse(fec);
        } catch (Exception e) {
        }

        return myDate;
    }
	
	public String timestampToString(Timestamp aFechaDD_SS) {
		try {
			Date myDate = new Date(aFechaDD_SS.getTime());
			SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
			String miString = myFormat.format(myDate);
			return miString;
		} catch (Exception e) {
		}
		return null;
	}

	private Vector<Object> diasSemana() {
		Vector<Object> dias = new Vector<>();
		dias.add("");
		dias.add("L");
		dias.add("M");
		dias.add("W");
		dias.add("J");
		dias.add("V");
		dias.add("S");
		dias.add("D");
		return dias;
	}

	@GetMapping(value = "/tramoMarca/porMarcaId/{id}")
	public List<TramoMarca> buscaTramosPorMarca(@PathVariable Integer id) {
		return serviceTMarca.buscaTramosPorMarca(id);
	}
	
	public Integer getIdTramo() {
		return idTramo;
	}

	public void setIdTramo(Integer idTramo) {
		this.idTramo = idTramo;
	}

}
