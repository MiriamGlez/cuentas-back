package cuentas.back.ccuentas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.AutobusExterno;
import cuentas.back.ccuentas.services.AutobusExternoServices;

@RestController
public class ControlAutobusExterno {
	
	private Integer busId = null;
	
	@Autowired
	private AutobusExternoServices servicesBusExt;
	
	@GetMapping("/busExt/consulta")
	public List<AutobusExterno> consultaBusExt(){ 
		return (List<AutobusExterno>) servicesBusExt.consultaBusExterno();
	}
	
	@GetMapping(value = "/bus/update/{idAutobus}")
	public ResponseEntity<AutobusExterno> actualiza(@PathVariable int idAutobus){
		Optional<AutobusExterno> bus = servicesBusExt.consultaId(idAutobus);
		AutobusExterno objBus = bus.get();
		setBusId(objBus.getIdAutobus());
		return new ResponseEntity<AutobusExterno>(objBus, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/busExt/eliminar/{idAutobus}")
	public ResponseEntity<String> elimina(@PathVariable int idAutobus){ 
		servicesBusExt.delete(idAutobus);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@PostMapping("/bus/save")
	public ResponseEntity<String> guarda(@RequestBody AutobusExterno bus){ 
		if(getBusId() != null) { 
			bus.setIdAutobus(getBusId());
		}else {
			setBusId(null);
		}
		servicesBusExt.guarda(bus);
		setBusId(null);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	public Integer getBusId() {
		return busId;
	}

	public void setBusId(Integer busId) {
		this.busId = busId;
	}
}
