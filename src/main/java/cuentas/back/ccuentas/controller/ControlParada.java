package cuentas.back.ccuentas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.Parada;
import cuentas.back.ccuentas.services.ParadaServices;

@RestController
public class ControlParada {

	private Integer idParada = null;
	
	@Autowired
	private ParadaServices servicesParada;
	
	@RequestMapping("/parada/busqueda/{variable},{variable2}")
	public List<Parada> buscaParadasPorCveDesc(@PathVariable String variable, @PathVariable String variable2) {
		System.out.println();
		return servicesParada.buscaParada(variable, variable2);
	}
	
	@GetMapping("/parada/lista")
	public List<Parada> listaParadas(){
		return (List<Parada>) servicesParada.listaParadas();
	}

	@PostMapping("/parada/guarda")
	public ResponseEntity<String> guarda(@RequestBody Parada p){
		if(getIdParada() !=null) { 
			p.setParadaId(getIdParada());
		}else { 
			setIdParada(null);
		}
		servicesParada.guardaParada(p);
		setIdParada(null);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/parada/update/{idParada}")
	private ResponseEntity<Parada> update(@PathVariable int idParada){ 
		Optional<Parada> p = servicesParada.consultaId(idParada);
		Parada objP = p.get();
		setIdParada(objP.getParadaId());
		return new ResponseEntity<Parada>(objP, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/parada/delete/{idParada}")
	public ResponseEntity<String> eliminaParada(@PathVariable int idParada){ 
		servicesParada.borraParada(idParada);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	public Integer getIdParada() {
		return idParada;
	}

	public void setIdParada(Integer idParada) {
		this.idParada = idParada;
	}
}
