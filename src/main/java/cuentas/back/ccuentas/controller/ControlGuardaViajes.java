package cuentas.back.ccuentas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.Viajes;
import cuentas.back.ccuentas.services.ViajesServices;

@RestController
public class ControlGuardaViajes {
	
	@Autowired
	private ViajesServices servicesViaje;

	@PostMapping("/viajes/save")
	public ResponseEntity<String> guarda(@RequestBody Viajes viaje){ 
		servicesViaje.guardaViajes(viaje);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
}
