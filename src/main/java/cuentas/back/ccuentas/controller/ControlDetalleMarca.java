package cuentas.back.ccuentas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.DetalleMarca;
import cuentas.back.ccuentas.services.DetalleMarcaServices;

@RestController
public class ControlDetalleMarca {

	@Autowired
	private DetalleMarcaServices servicesDetalle;
	
	@PostMapping("/detalleMarca/guarda")
	public ResponseEntity<String> guardaDetalle(@RequestBody DetalleMarca detalle){ 
		servicesDetalle.guarda(detalle);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
