package cuentas.back.ccuentas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cuentas.back.ccuentas.modelo.Asociados;
import cuentas.back.ccuentas.services.AsociadoServices;

@RestController
public class ControlAsociados {

	private Integer asociadoId = null;
	
	@Autowired
	private AsociadoServices servicesAsociado;
	
	@GetMapping("/socio/consulta")
	public List<Asociados> consultaASociados(Model model){
		return servicesAsociado.consultar();
	}
	
	@GetMapping(value = "/socio/update/{asociadoId}")
	public ResponseEntity<Asociados> actualiza(@PathVariable int asociadoId){
		Optional<Asociados> asociado = servicesAsociado.consultarId(asociadoId);
		Asociados socio = asociado.get();
		setAsociadoId(socio.getAsociadoId());
		return new ResponseEntity<Asociados>(socio, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/socio/eliminar/{asociadoId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> elimina(@PathVariable int asociadoId){
		servicesAsociado.delete(asociadoId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/socio/save")
	public ResponseEntity<String> guardar(@RequestBody Asociados asociado){ 
		if(getAsociadoId() != null) { 
			asociado.setAsociadoId(getAsociadoId());
		}
		servicesAsociado.guarda(asociado);
		setAsociadoId(null);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}


	public Integer getAsociadoId() {
		return asociadoId;
	}

	public void setAsociadoId(Integer asociadoId) {
		this.asociadoId = asociadoId;
	}
	
}
