package cuentas.back.ccuentas.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.DetalleMarca;
import cuentas.back.ccuentas.repositorio.IDetalleMarca;

@Service
public class DetalleMarcaServices {

	@Autowired
	private IDetalleMarca data;
	
	public List<DetalleMarca> consultaDetalleMarcaIds(Integer marcaId, Integer marcaRolId){
		return null;
	}

	public int guarda(DetalleMarca detalle) {
		DetalleMarca de = data.save(detalle);
		if(!de.equals(null)) {
			return 1;
		}
		return 0;
		
	}
	
}
