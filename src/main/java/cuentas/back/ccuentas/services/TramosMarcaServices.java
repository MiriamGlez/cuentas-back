package cuentas.back.ccuentas.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.ccuentas.repositorio.ITramosMarca;

@Service
public class TramosMarcaServices {

	@Autowired
	private ITramosMarca data;

	public List<TramoMarca> consultar() {
		return (List<TramoMarca>) data.findAll();
	}

	public TramoMarca guarda(TramoMarca tramoM) {

		TramoMarca t = data.save(tramoM);
		if (!t.equals(null)) {
			return t;
		}
		return t;
	}

	public Optional<TramoMarca> listarLlave(Integer id){ 
		return data.findById(id);
	}
	
	public void delete(int id) { 
		data.deleteById(id);
	}

	public List<TramoMarca> buscaTramosPorMarca(Integer id) {
		return (List<TramoMarca>) data.buscaTramosPorMarca(id);
	}
}
