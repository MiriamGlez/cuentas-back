package cuentas.back.ccuentas.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.Parada;
import cuentas.back.ccuentas.repositorio.IParada;

@Service
public class ParadaServices {
	
	@Autowired
	private IParada data;
	
	public List<Parada> buscaParada(String clave, String descripcion) { 
		return data.buscaParadaPorCveDescripcion("%"+clave+"%", "%"+clave+"%");
	}
	
	public List<Parada> listaParadas(){ 
		List<Parada> list = new ArrayList<Parada>();
		Iterator<?> it = data.findAll().iterator();
		while(it.hasNext()) { 
			Parada p = (Parada) it.next();
			String descripcion = recuperaEstatus(p.getStatus());
			boolean val = recuperaBolEstatus(p.getStatus());
			p.setDescEstatus(descripcion);
			p.setValorEstatus(val);
			list.add(p);
		}
		
		return list;
	}
	
	private boolean recuperaBolEstatus(Integer status) {
		if(status.intValue() == 1) { 
			return true;
		}else if(status.intValue() == 0) {
			return false;
		}
		return false;
	}

	private String recuperaEstatus(Integer status) {
		if(status.intValue() == 1) { 
			return "ACTIVO";
		}else if(status.intValue() == 0) {
			return "INACTIVO";
		}
		return "";
	}

	public int guardaParada(Parada parada) { 
		Parada p = data.save(parada);
		if(!p.equals(null)) {
			return 1;
		}
		return 0;
	}
	
	public Optional<Parada> consultaId(int id){ 
		return data.findById(id);
	}
	
	public void borraParada(int id) { 
		data.deleteById(id);
	}
	
}

