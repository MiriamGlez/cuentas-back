package cuentas.back.ccuentas.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.ccuentas.repositorio.IMarcaRol;
import cuentas.back.general.modelo.Usuario;

@Service
public class MarcaRolServices {

	@Autowired
	private IMarcaRol data;
		
	public List<MarcaRol> consultar() {
		List<MarcaRol> list = new ArrayList<>();
		Iterator<?> it = data.findAll().iterator();
		while(it.hasNext()) { 
			MarcaRol marca = (MarcaRol)it.next();
			String desc = recuperaStatus(marca.getEstatus());
			marca.setDescStatus(desc);
			list.add(marca);
		}
		return list;
	}

	private String recuperaStatus(Integer estatus) {
		if(estatus.intValue() == 0){ 
			return "INACTIVO";
		}else if(estatus.intValue() == 1) {
			return "ACTIVO";
		}
		return "";
	}

	public int guardar(MarcaRol marcaRol) {
		Usuario usuario = creaUsuario();
		
		int value = 0;
		marcaRol.setEstatus(new Integer(1));
		marcaRol.setFecHorAct(new Date());
		marcaRol.setUsuario(usuario);
		MarcaRol m = data.save(marcaRol);
		if(!m.equals(null)) { 
			value = 1;
		}
		return value;
	}

	private Usuario creaUsuario() {
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(new Integer(3));
		return usuario;
	}

	public Optional<MarcaRol> listarId(int id){
		return data.findById(id);
	}
	
	public void delete(int id) {
		data.deleteById(id);
	}

	public MarcaRol buscaMarcaRolPorDescripcion(String descripcionMarcarol) {
		return data.buscaMarcaRolPorDescripcion(descripcionMarcarol);
	}
}
