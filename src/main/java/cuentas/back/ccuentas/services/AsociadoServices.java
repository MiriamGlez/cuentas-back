package cuentas.back.ccuentas.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.Asociados;
import cuentas.back.ccuentas.repositorio.IAsociados;

@Service
public class AsociadoServices {

	@Autowired
	private IAsociados data;
	
	public List<Asociados> consultar(){ 
		List<Asociados> list = new ArrayList<Asociados>();
		Iterator<?> it = data.findAll().iterator();
		while(it.hasNext()) { 
			Asociados socio = (Asociados)it.next();
			String descripcion = recuperaEstatus(socio.getEstatus());
			socio.setDescEstatus(descripcion);;
			list.add(socio);
		}
		return list;
	}
	
	private String recuperaEstatus(Integer estatus) {
		if(estatus.intValue() == 1) {
			return "ACTIVO";
		}else if(estatus.intValue() == 0) { 
			return "INACTIVO";
		}
		return "";
	}

	public Optional<Asociados> consultarId(int id){ 
		return data.findById(id);
	}
	
	public void delete(int id){ 
		data.deleteById(id);
	}
	
	public int guarda(Asociados asociado) { 
		int value = 0;
		asociado.setFechoract(new Date());
		asociado.setUsuarioId(new Integer(3));
		
		Asociados a = data.save(asociado);
		
		if(!a.equals(null)) {
			value=1;
		}
		return value;
	}
}
