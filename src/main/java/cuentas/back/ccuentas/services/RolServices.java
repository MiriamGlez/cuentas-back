package cuentas.back.ccuentas.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.Rol;
import cuentas.back.ccuentas.repositorio.IRol;

@Service
public class RolServices {
	
	@Autowired
	private IRol data;

	public Rol recuperaRol(Integer marcaId, Integer marcaRolId, Date fecha) {
		return data.recuperaRol(marcaId, marcaRolId, fecha);
	}
	
	public Rol guardaRol(Rol rol) {
		return data.save(rol);
	}
}
