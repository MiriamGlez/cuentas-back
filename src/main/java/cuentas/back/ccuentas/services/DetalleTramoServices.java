package cuentas.back.ccuentas.services;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.DetalleTramo;
import cuentas.back.ccuentas.repositorio.IDetalleTramo;

@Service
public class DetalleTramoServices {
	
	@Autowired
	private IDetalleTramo data;
	
	public List<DetalleTramo> detalleListaPorTramo(Integer idTramo){ 
		return data.buscaDetallePorTramo(idTramo);
	}
	
	public void guardaDetalle(Vector<Object> lstDetalles) { 
		Iterator<?> it = lstDetalles.iterator();
		while(it.hasNext()) { 
			DetalleTramo detalle = (DetalleTramo) it.next();
			data.save(detalle);
		}
	}

	public List<DetalleTramo> consultar() {
		return (List<DetalleTramo>) data.findAll();
	}

}
