package cuentas.back.ccuentas.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.AutobusExterno;
import cuentas.back.ccuentas.repositorio.IAutobusExterno;

@Service
public class AutobusExternoServices {

	@Autowired
	private IAutobusExterno data;
	
	public List<AutobusExterno> consultaBusExterno(){ 
		return (List<AutobusExterno>) data.findAll();
	}
	
	public int guarda(AutobusExterno busExterno) {
		System.out.println();
		AutobusExterno bus = data.save(busExterno);
		if(!bus.equals(null)) { 
			return 1;
		}
		return 0;
	}
	
	public Optional<AutobusExterno> consultaId(int id){ 
		return data.findById(id);
	}
	
	public void delete(int id) { 
		data.deleteById(id);
	}
}
