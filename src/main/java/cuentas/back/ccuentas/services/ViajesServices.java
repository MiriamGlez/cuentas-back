package cuentas.back.ccuentas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cuentas.back.ccuentas.modelo.Viajes;
import cuentas.back.ccuentas.repositorio.IViajes;

@Service
public class ViajesServices {

	@Autowired IViajes data;
	
	public int guardaViajes(Viajes viaje) { 
		Viajes v = data.save(viaje);
		if(!v.equals(null)) {
			return 1;
		}
		return 0;
	}
	
}
