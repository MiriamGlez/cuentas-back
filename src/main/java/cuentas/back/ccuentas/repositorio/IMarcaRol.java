package cuentas.back.ccuentas.repositorio;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.MarcaRol;

@Repository
public interface IMarcaRol extends CrudRepository<MarcaRol, Integer> {

	@Query("select x from MarcaRol x  where x.descripcion like ?1")
	MarcaRol buscaMarcaRolPorDescripcion(String descripcionMarcarol);

}
