package cuentas.back.ccuentas.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.DetalleTramo;

@Repository
public interface IDetalleTramo extends CrudRepository<DetalleTramo, Integer>{

	@Query("select t from DetalleTramo t where t.key.tramoId = ?1")
	List<DetalleTramo> buscaDetallePorTramo(Integer idTramo);
}
