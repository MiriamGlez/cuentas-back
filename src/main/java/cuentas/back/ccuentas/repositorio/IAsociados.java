package cuentas.back.ccuentas.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.Asociados;

@Repository
public interface IAsociados extends CrudRepository<Asociados, Integer>{

}
