package cuentas.back.ccuentas.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.Parada;

@Repository
public interface IParada extends CrudRepository<Parada, Integer>{

	@Query("select p from Parada p where p.cveParada like ?1 or p.descParada like ?2 ")
	List<Parada> buscaParadaPorCveDescripcion(String clave, String descripcion);
}
