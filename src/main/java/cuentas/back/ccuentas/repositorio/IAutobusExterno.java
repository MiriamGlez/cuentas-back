package cuentas.back.ccuentas.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.AutobusExterno;

@Repository
public interface IAutobusExterno extends CrudRepository<AutobusExterno, Integer>{

}
