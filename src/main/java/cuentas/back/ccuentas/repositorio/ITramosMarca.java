package cuentas.back.ccuentas.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.TramoMarca;

@Repository
public interface ITramosMarca extends CrudRepository<TramoMarca, Integer>  {

	@Query("select x from TramoMarca x where x.estatus = 1 and x.marca.id = ?1")
	List<TramoMarca> buscaTramosPorMarca(Integer id);
}
