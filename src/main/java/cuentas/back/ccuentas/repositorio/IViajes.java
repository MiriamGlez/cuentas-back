package cuentas.back.ccuentas.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.Viajes;

@Repository
public interface IViajes extends CrudRepository<Viajes, Integer>{

}
