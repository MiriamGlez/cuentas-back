package cuentas.back.ccuentas.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.DetalleMarca;

@Repository
public interface IDetalleMarca extends CrudRepository<DetalleMarca, Integer>{

	@Query("select x from DetalleMarca x where x.key.marca.id = ?1 and x.key.marcaRol.rolId = ?2 ")
	List<DetalleMarca> detallePorMarca(Integer idMarca, Integer idMarcaRol);

}
