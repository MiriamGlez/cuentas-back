package cuentas.back.ccuentas.repositorio;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cuentas.back.ccuentas.modelo.Rol;

@Repository
public interface IRol extends CrudRepository<Rol, Integer> {

	@Query("select x from Rol x where x.marcaId = ?1 and x.marcaRolId = ?2 and ?3 BETWEEN x.periodoInicial and x.periodoFinal ")
	Rol recuperaRol(Integer marcaId, Integer marcaRolId, Date fecha);

}
